Name = input("enter your name : ")
subjects = []
grades = []
points = 0
gender = input("enter your gender: ")
title = ""
fail = ""
if gender.capitalize() == "Male":
    title = "Mr."
elif gender.capitalize() == "Female":
    title = "Ms."

for x in range(1, 7):
    subject_gr_list = []
    subject_grade = input()
    subject_gr_list = subject_grade.split()

    if (subject_gr_list[0] == "English" or "english") and (subject_gr_list[1] == "U"):
        fail = "True"
    else:
        fail = "False"
    subjects.append(subject_gr_list[0].capitalize())
    grades.append(subject_gr_list[1])


grade_num = []
for i in grades:
    if i.upper() == "1":
        points += 10
        grade_num.append(10)
    elif i.upper() == "2":
        points += 9
        grade_num.append(9)
    elif i.upper() == "3":
        points += 8
        grade_num.append(8)
    elif i.upper() == "4":
        points += 7
        grade_num.append(7)
    elif i.upper() == "U":
        points += 0
        grade_num.append(0)
    elif i.upper() == "A+":
        points += 8
        grade_num.append(8)
    elif i.upper() == "A":
        points += 7
        grade_num.append(7)
    elif i.upper() == "B":
        points += 6
        grade_num.append(6)
    elif i.upper() == "C":
        points += 5
        grade_num.append(5)
    elif i.upper() == "D":
        points += 4
        grade_num.append(4)
    elif i.upper() == "E":
        points += 3
        grade_num.append(3)
    elif i.upper() == "F":
        points += 2
        grade_num.append(2)
    elif i.upper() == "G":
        points += 1
        grade_num.append(1)

    else:
        print("Incorrect symbol detected {}".format(i))
Field_of_studies = []

for i in range(2):
    Filed_study = input("Field of study {i}: ".format(i+1))
    Field_of_studies.append(Filed_study)

points = points - min(grade_num)

if (points >= 26) and (fail == "False"):
    print("Outcome  Dear {} {}, you have {} points in 5 subjects. Congratulations you are eligible for tertiary studies in Namibia.".format(title, Name, points))

else:
    print("Unfortunately you do not qualify for tertiary education")

for i in range(2):
    if Field_of_studies[i] == "Management Sciences":
        if points < 26:
            print("You do not qualify as your points are to low")
        elif "Mathematics" not in subjects:
            print("Comment: Mathematics missing on list of subjects")
        elif "English" not in subjects:
            print("Comment: English missing on list of subjects")
        elif (grades[subjects.index("Mathematics")] == ("F" or "U" or "G")):
            print("You need at least a E to qualify in mathematics ")
        elif (grades[subjects.index("English")] == ("F" or "U" or "G" or "E") ):
            print("You need at least a D to qualify")
        else:
            print("You meet all necessary requirements")

    if Field_of_studies[i] == "Human Sciences":
        if points < 26:
            print("You do not qualify as your points are to low")

        elif "English" not in subjects:
            print("Comment: English missing on list of subjects")

        elif (grades[subjects.index("English")] == ("F" or "U" or "G" or "E" or "D") ):
            print("You need at least a C to qualify")
        else:
            print("You meet all necessary requirements")

    if Field_of_studies[i] == "Computing and Informatics":
        if points < 30:
            print("You do not qualify as your points are to low")

        elif "Mathematics" not in subjects:
            print("Comment: English missing on list of subjects")

        elif (grades[subjects.index("Mathematics")] == ("F" or "U" or "G" or "E" or "D")):
            print("You need at least a C to qualify")
        else:
            print("You meet all necessary requirements")

    if Field_of_studies[i] == "Engineering Sciences":
        if points < 37:
            print("You do not qualify as your points are to low")

        elif "Mathematics" not in subjects:
            print("Comment: Mathematics missing on list of subjects")

        elif "Physical Science" not in subjects:
            print("Comment: Physical Science missing on list of subjects")

        elif "English " not in subjects:
            print("Comment: English  missing on list of subjects")

        elif (grades[subjects.index("Mathematics")] != ("1" or "2" or "3")):
            print("You need at least a 3 to qualify")

        elif (grades[subjects.index("PhysicalScience")] != ("1" or "2" or "3" or "4")):
            print("You need at least a 4 to qualify")

        elif (grades[subjects.index("English ")] != ("1" or "2" or "3" or "4")):
            print("You need at least a 4 to qualify")
        else:
            print("You meet all necessary requirements")

    if Field_of_studies[i] == "Health and Applied Sciences":
        if points < 30:
            print("You do not qualify as your points are to low")

        elif "Mathematics" not in subjects:
            print("Comment: Mathematics missing on list of subjects")

        elif "Physical Science" not in subjects:
            print("Comment: Physical Science missing on list of subjects")

        elif "English " not in subjects:
            print("Comment: English  missing on list of subjects")

        elif (grades[subjects.index("English")] != ("F" or "G" or "U" or "E" or "D")):
            print("You need at least a C to qualify")

        elif (grades[subjects.index("Biology")] != ("F" or "G" or "U" or "E")):
            print("You need at least a D to qualify")


        elif (grades[subjects.index("PhysicalScience")] != ("F" or "G" or "U" or "E")):
            print("You need at least a D to qualify")

        elif (grades[subjects.index("Mathematics")] != ("F" or "G" or "U" or "E")):
            print("You need at least a D to qualify")
        else:
            print("You meet all necessary requirements")

    if Field_of_studies[i] == "Natural Resources and Spatial Sciences":
        if points < 30:
            print("You do not qualify as your points are to low")

        elif "Mathematics" not in subjects:
            print("Comment: Mathematics missing on list of subjects")

        elif "Geography" not in subjects:
            print("Comment: Geography missing on list of subjects")

        elif "English " not in subjects:
            print("Comment: English  missing on list of subjects")

        elif (grades[subjects.index("English")] != ("F" or "G" or "U" or "E" or "D")):
            print("You need at least a C to qualify")

        elif (grades[subjects.index("Geography")] != ("F" or "G" or "U" or "E")):
            print("You need at least a D to qualify")

        elif (grades[subjects.index("Mathematics")] != ("F" or "G" or "U" or "E")):
            print("You need at least a D to qualify")
        else:
            print("You meet all necessary requirements")
