print("Ministry of Home Affairs ID and PASSPORT Generator: ")
print("")

# Prompts the user to enter their personal information and a the five-digit postfix
personal_details = input("Enter your first name, date of birth(format YYYY-MM-DD) and five-digit postfix: ")
personal_details_list = []

# It then seperates the users inputs into a list
personal_details_list = personal_details.split()

# We then set the variable Date_of_Birth equal to the second element in the list which contains the date of birth of the user
Date_of_Birth = personal_details_list[1]

# This variable is used for the Date_of_Birth without the hyphen
DOB_no_hyphen = ""

# This loop iterates through the Date_of_Birth and if a hyphen is encountered the if statement discards it
for i in Date_of_Birth :
    if i != "-":
        DOB_no_hyphen += str(i)

# The Id is then set equal to the DOB without the first two digits and the five-digit postfix
ID = "{} {}".format(DOB_no_hyphen[2:], personal_details_list[2])
# The passport code is then set equal to the DOB without the first two digits and only the last 3 characters of the five-digit postfix
passport = "P-NAM {} {} ".format(ID[0:6], ID[10:])


# The information is then Displayed
print("Good day {} welcome to the Ministry of Home Affairs and Immigration automated system.".format(personal_details_list[0]))
print("Your ID number is : {}".format(ID))
print("Passport code : {}".format(passport))
