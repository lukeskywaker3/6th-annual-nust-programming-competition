import random

#These are the variables and lists that will be used in this program
num_questions = int(input(""))
score = 0
memo = []
answer = ""
num_students = 0
count = 0
student_answer = []
id_list = []
student_results = {}
student_marks = {}
grade_id = {}
score_list = []

#This asks the teachers for the number of questions and sets the answers
for i in range(num_questions):
    answer = input("")
    memo += answer


#This asks the teacher for the number of students that will take the quiz
num_students = int(input(""))
#This asks the students for their id number and asnwers to the quiz
while count < num_students:
    id = int(input(""))
    id_list.append(id)
    student_answer = []
    for i in range(num_questions):
        stu_answer = input("")
        student_answer += stu_answer

    student_results.update({id: student_answer})
    count +=1


#This compares the memo and the students answers awarding a mark for each correct match
count = 0
while count < num_students:
    q = 0
    score = 0
    for i in student_results[id_list[count]]:
        if i == memo[q]:
            score += 1
        else:
            score = score
        q += 1

    score_list.append(score)
    student_marks.update({id_list[count]: score})
    grade_id.update({score:id_list[count]})
    count += 1


choice = input("")


# This sorts the scores and id lists for the arrangement of each display
id_list.sort()
score_list.sort()


#This asks the teacher to enter how they want the results to be ordered when displayed
if choice == "LEARNER_ID_ASC":
    for i in id_list:
        print("{} {}".format(i, student_marks[i]))

elif choice == "LEARNER_ID_DESC":
    id_list = id_list[::-1]
    for i in id_list:
        print("{} {}".format(i, student_marks[i]))

elif choice == "GRADE_ASC":
    for i in score_list:
        print("{} {}".format(grade_id[i], i))

elif choice == "GRADE_DESC":
    score_list = score_list[::-1]
    for i in score_list:
        print("{} {}".format(grade_id[i], i))
