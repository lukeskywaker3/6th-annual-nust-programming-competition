print("DECADES OR LUSTRUM FIND OUT?")

# These are the variables which we are going to use in our code and have intialized them
age = 0
rounded_age = 0
string_age = ""
# This enables the user input their full name and age( in days)
full_name = input("Enter Full Name: ")
age = int(input("Enter age in total number of days: "))

# The age is then converted to years and the function round removes the decimal place from the previous calculation
age = age/365
rounded_age = round(age)

print("Good day {}, you are {} years old.".format(full_name, rounded_age))

# This is where the year is calculated and we converted age to string to get the first digit of the users calculated age
year = 2021 - rounded_age
string_age = str(rounded_age)


# If statement to accommadate the diffrent ages of each user and then returns diffrent outputs based on the users age
if rounded_age >= 10:
    print("That means you have lived for over {} decades and you were born in {}".format(string_age[0], year))

elif (rounded_age < 10) and (rounded_age >= 5):
    print("That means you have lived for over {} lustrum and you were born in {}".format(string_age[0], year))
# This statment only applies to ages<5 years of age
else:
    print("You have not been alive long enough to be considered for a lustrum")